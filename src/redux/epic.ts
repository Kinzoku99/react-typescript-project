import { filter, map } from "rxjs";
import { setCurrent, setNumbers } from "./reducer";

const clearInputEpic = (action$: any) => action$.pipe(
    filter(setNumbers.match),
    map(() => setCurrent("")),
);

export default clearInputEpic;