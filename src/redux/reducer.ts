import { createSlice } from "@reduxjs/toolkit";
import { PayloadAction } from "@reduxjs/toolkit";
import { Option, Options } from "../logic/options";
import { RootState } from "./store";
import sort from "../logic/sort";

const fixInput = (input: string) => (
    (number: number) => isNaN(number) || number > 999999999999999  
    ? NaN : Math.round(number))
    (parseInt(input, 10));

interface State {
    current: number,
    numbers: number[],
    options: Options
};

//type SideEffect = [Option, (numbers: number[]) => number[]];
//type setNumbersPayload = [number[], SideEffect[]];

const initialState: State = {
    current: NaN,
    numbers: [],
    options: new Options(),
};

const getTransform = (options: Options) => {
    const doSideEffects = options.getSideEffect();
    return (numbers: number[]) => numbers.length === 0 ? [] : doSideEffects(sort(numbers));
};

const numbersSlice = createSlice({
    name: 'numbers',
    initialState,
    reducers: {
        setNumbers: (state: RootState, action: PayloadAction<number[]>) => {
            const newNumbers = action.payload;
            const transform = getTransform(state.options);
            state.numbers = transform([...state.numbers, ...newNumbers].filter((n: number) => !isNaN(n)));
        },
        mapNumbers: (state: RootState, action: PayloadAction<(number: number) => number>) => {
            const transform = getTransform(state.options);
            state.numbers = transform(state.numbers.map(action.payload));
        },
        switchOption: (state: RootState, action: PayloadAction<Option>) => {
            state.options.switch(action.payload)();
        },
        setCurrent: (state: RootState, action: PayloadAction<string>) => {
            state.current = fixInput(action.payload);
        },
        clear: (state: RootState) => {
            state.numbers = [];
        }
    }
});

export const { setNumbers, mapNumbers, switchOption, setCurrent, clear } = numbersSlice.actions;
export type { State };

export default numbersSlice.reducer;