import { configureStore } from "@reduxjs/toolkit";
import numbersReducer from "./reducer";
import clearInputEpic from "./epic";
import { createEpicMiddleware } from "redux-observable";

const epicMiddleware = createEpicMiddleware();

// FIX THIS ANY
const store: any = configureStore({
    reducer: {
        numbersReducer: numbersReducer,
    },
    middleware: middleware => middleware({serializableCheck: false}).concat(epicMiddleware)
});

epicMiddleware.run(clearInputEpic);

type RootState = ReturnType<typeof store.getState>;
type AppDispatch = typeof store.dispatch;

export { store };
export type { RootState, AppDispatch };