import { RootState } from "./store";

const selectNumbers = (state: RootState) => state.numbersReducer.numbers;

const selectCurrent = (state: RootState) => state.numbersReducer.current;

export { selectNumbers, selectCurrent }; 