import { FC, useCallback } from 'react';
import '../App.css';
import Display from './Display/Display';
import ActionPanel from './Options/ActionPanel';
import Navigation from './Navigation';
import { useDispatch } from 'react-redux';
import { clear } from '../redux/reducer';

const App: FC = () => {
    const dispatch = useDispatch();
    const clearAction = useCallback(() => {dispatch(clear());},[dispatch]);
  return (
    <div>
        <Navigation clearAction={clearAction}/>
        <ActionPanel />
        <Display />
    </div>
  );
}

export default App;
