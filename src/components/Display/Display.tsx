import { FC } from "react";
import { ListGroup } from "react-bootstrap";
import { useSelector } from "react-redux";
import { selectNumbers } from "../../redux/selectors";

const Display: FC = () => (
    <div>
        <ListGroup className="mx-5 mb-4">
            {useSelector(selectNumbers).map((number: number, index: number) => (
                <ListGroup.Item key={index} variant={index % 2 === 0 ? "light" : "dark"}>
                    {number}
                </ListGroup.Item>
            ))}
        </ListGroup>
    </div>
);

export default Display;