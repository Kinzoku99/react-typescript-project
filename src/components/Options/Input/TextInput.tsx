import { FC } from "react";
import { Form } from "react-bootstrap";

//FIX ANY
const TextInput: FC<any> = ({changeCurrent, inputValue}) => (
    <Form.Control
        type="text"
        className="mb-3"  
        placeholder="0" 
        autoComplete="off" 
        value={inputValue} 
        onInput={changeCurrent}
    />
);

export default TextInput;