import { FC } from "react";
import { ButtonGroup, Form } from "react-bootstrap";
import TextInput from "./TextInput";
import ButtonComponent from "./ButtonComponent";

export type button = [string, string, () => void];

type InputComponentType = FC<{
    buttons: button[], 
    changeCurrent: (event: React.KeyboardEvent<HTMLInputElement>) => void,
    inputValue: string
}>;

const InputComponent: InputComponentType = ({buttons, changeCurrent, inputValue}) => (
    <Form.Group className="mb-3">
        <TextInput changeCurrent={changeCurrent} inputValue={inputValue}/>
        <ButtonGroup className="d-flex">
            {buttons.map((button, index) => (
                <ButtonComponent key={index} onClick={button[2]} text={button[1]} />
            ))}
        </ButtonGroup>
    </Form.Group>
);

export default InputComponent;