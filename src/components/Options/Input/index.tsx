import { useCallback, FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PayloadAction } from "@reduxjs/toolkit";
import { setNumbers, setCurrent, mapNumbers } from "../../../redux/reducer";
import { selectCurrent } from "../../../redux/selectors";
import InputComponent, {button} from "./InputComponent";
import getFibonacciSeq from "../../../logic/fibonacci";

type Action = PayloadAction<number[]> | PayloadAction<(number: number) => number>;

const Input: FC = () => {
    const dispatch = useDispatch();
    const currentNumber: number = useSelector(selectCurrent);

    const changeCurrent = useCallback((event: React.KeyboardEvent<HTMLInputElement>) => {
        dispatch(setCurrent(event.target.value));
    }, [dispatch]);

    const $callback = (action: Action) => useCallback(() => {dispatch(action)}, [action]);

    const buttons: button[] = [
        ["new-button", "Add new number", $callback!(setNumbers([currentNumber]))],
        ["fibonacci", "Add fibonacci", $callback!(setNumbers(getFibonacciSeq(currentNumber)))],
        ["square-button", "Square All", $callback!(mapNumbers(x => x * x))]
    ];
    
    return <InputComponent 
        buttons={buttons}
        changeCurrent={changeCurrent} 
        inputValue={isNaN(currentNumber) ? "" : `${currentNumber}`}
    />;
};

export default Input;