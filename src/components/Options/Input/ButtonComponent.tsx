import { FC } from "react";
import Button from "react-bootstrap/esm/Button";

type ButtonComponentProps = {onClick: () => void, text: string};

const ButtonComponent: FC<ButtonComponentProps> = ({onClick, text}) => (
        <Button
            variant="secondary" 
            type="button" 
            onClick={onClick} >
            {text}
        </Button>
);

export default ButtonComponent;