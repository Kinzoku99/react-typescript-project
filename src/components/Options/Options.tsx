import { FC, useCallback } from "react";
import { useDispatch } from "react-redux";
import { switchOption } from "../../redux/reducer";
import { Option } from "../../logic/options";
import { Form } from "react-bootstrap";

const Options: FC = () => {
    const dispatch = useDispatch();

    const changeDuplicates = useCallback(() => {
        dispatch(switchOption(Option.Duplicates));
    }, [dispatch]);

    const changeNoparsable = useCallback(() => {
        dispatch(switchOption(Option.Parsable));
    }, [dispatch]);

    return (
        <Form.Group className="mb-3" controlId="options">
            <Form.Label>Options</Form.Label>
            <Form.Check type="checkbox" label="Remove Duplicates" onChange={changeDuplicates} defaultChecked/>
            <Form.Check type="checkbox" label="Remove Noparsable" onChange={changeNoparsable} defaultChecked/>
        </Form.Group>
    )
};

export default Options;