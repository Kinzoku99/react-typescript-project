import { FC } from "react";
import Options from "./Options";
import Input from "./Input";
import { Container, Form } from "react-bootstrap";

const ActionPanel: FC = () => (
    <Container fluid className="p-5">
        <Form>
            <Input />
            <Options />
        </Form>
    </Container>
);

export default ActionPanel;