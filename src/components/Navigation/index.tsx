import { FC } from "react";
import { Button, Container, Navbar } from "react-bootstrap";

type NavComponent = FC<{clearAction: () => void}>;

const Navigation: NavComponent = ({clearAction}) => (
    <Navbar bg="dark" variant="dark">
        <Container>
            <Navbar.Brand>
                Merge Sorted Number List
            </Navbar.Brand>
            <Navbar.Text>
                <Button onClick={clearAction}>
                    Clear List
                </Button>
            </Navbar.Text>
        </Container>
    </Navbar>
);

export default Navigation;