import matrix from "./matrix";

const getFibonacci = () => new matrix(2, (x, y) => x + y === 2 ? 0 : 1);

const getThree = (matrix: matrix, howMany: number) => 
    ((table : [number, number, number]) => howMany === 3 ? table : table.splice(0, howMany))
    ([matrix.at(1,1), matrix.at(0,1), matrix.at(0,0)]);

const getFibonacciSeq = (number: number) => {
        const validNumber: boolean = !isNaN(number) && number <= 1000 && number > 0;
        if (!validNumber) return [];

        let fibonacci = getFibonacci();  
        const third = fibonacci.power(3);
        let newNumbers: number[] = [];

        for (let it = 0; it * 3 < number; it++) {
            const howMany = number - it * 3 > 3 ? 3 : number - it * 3;
            newNumbers = newNumbers.concat(getThree(fibonacci, howMany));
            fibonacci = fibonacci.multiply(third);
        }

        return newNumbers;
};

export default getFibonacciSeq;