enum Option {
    Duplicates, 
    Parsable
};

type NumberFunction = (numbers: number[]) => number[];

const uniqueNumbers: NumberFunction = numbers => 
    ((seen: any) => numbers.filter(item => seen.hasOwnProperty(item) ? false : (seen[`${item}`] = true)))({});

const parsableNumbers: NumberFunction = numbers => 
    (isParsable => numbers.filter(isParsable))
    ((number: number) => !isNaN(number) && number === parseInt(`${number}`, 10))

class Options {
    private duplicates: boolean;
    private parsable: boolean;

    constructor() {
        this.duplicates = true;
        this.parsable = true;
    }

    switch(option: Option) {
        return (
            option === Option.Duplicates
            ? () => {this.duplicates = !this.duplicates;}
            : option === Option.Parsable
            ? () => {this.parsable = !this.parsable;}
            : () => {console.log("this shouldn't be shown");}
        );
    }

    getSideEffect(): NumberFunction {
        const Idenity: NumberFunction = x => x;
        let sideEffects: NumberFunction[] = [];

        if (this.duplicates) sideEffects.push(uniqueNumbers);

        if (this.parsable) sideEffects.push(parsableNumbers);

        return sideEffects.reduce((acc, curr) => numbers => curr(acc(numbers)), Idenity);
    }
};

export {Option, Options};