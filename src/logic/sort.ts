class list {
    private array : number[]
    private it : number

    constructor(array: number[]) {
        this.array = array; 
        this.it = 0;
    }

    public ended = () => this.it >= this.array.length;
    public next = () => this.ended() ? NaN : this.array[this.it++];
    public get = () => this.array;
    public smaller = (other: list) => this.array[this.it] < other.array[other.it];
    public compare = (other: list) => other.ended() || (!this.ended() && this.smaller(other));

    static choose(left: list, right: list) {
        return left.compare(right) ? left.next() : right.next();
    }
};


type MergeType = (left: list, right: list) => number[];
type SortType = (array : number[], start?: number, end?: number) => number[];

const merge: MergeType = (left, right) => 
    left.ended() && right.ended() ? [] : [list.choose(left, right), ...merge(left, right)];

const sort: SortType = (array, start = 0, end = array.length - 1) => 
    start === end 
    ? [array[start]]
    : (middle => merge(
        new list(sort(array, start, middle)), 
        new list(sort(array, middle + 1, end))
    ))(Math.floor((start + end) / 2));

export default sort;