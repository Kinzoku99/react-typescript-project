type Pattern = (x: number, y: number, prev: number) => number;

class matrix {
    private content: number[][];
    private dim: number;

    private checkMatrix(other: matrix) {
        const can_multiply = other.getDim() === this.dim;
        console.assert(can_multiply);
    }

    constructor(dim: number, pattern?: Pattern) {
        this.dim = dim;
        this.content = Array(dim).fill(Array(dim).fill(0));

        if (pattern)
            this.setPatern(pattern);
    }

    getDim(): number {
        return this.dim;
    }
    
    get(): number[][] {
        return this.content;
    }

    setPatern(pattern: Pattern): void {
        this.content = this.content.map((row, x) => row.map((element, y) => pattern(x, y, element)));
    }

    static getIdentity(dim: number): matrix {
        const identityPattern: Pattern = (x, y) => Number(x === y);
        return new matrix(dim, identityPattern);
    }

    at(x: number, y: number): number {
        return this.content[x][y];
    }

    add(other: matrix): matrix {
        this.checkMatrix(other);
        const sumUp: Pattern = (x, y) => this.at(x, y) + other.at(x, y)
        return new matrix(this.dim, sumUp);
    }

    multiply(other: matrix): matrix {
        this.checkMatrix(other);

        const sumUp = (prev: number, curr: number) => prev + curr;
        const mulCoresponding = (x: number, y: number, k: number) => this.at(x,k) * other.at(k,y);
        const newArray = Array(this.dim).fill(0);
        const mulMatrices: Pattern = (x, y) => newArray.map((_,k) => mulCoresponding(x, y, k)).reduce(sumUp);

        return new matrix(this.dim, mulMatrices);;
    }

    power(n: number): matrix {
        if (n <= 0)
            return matrix.getIdentity(this.dim);
        else if(n % 2 === 0)
            return (x => x.multiply(x))(this.power(n / 2));
        else
            return this.multiply(this.power(n - 1));
    }

    clone(): matrix {
        const clonePattern: Pattern = (x, y) => this.at(x, y);
        return new matrix(this.dim, clonePattern);
    }
}

export default matrix;